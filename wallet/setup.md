## Set up the wallet

``` bash
mkdir -p $ORACLE_HOME/wallets/stripe/
cd $ORACLE_HOME/wallets/stripe
#this misses one cert :( echo -n | openssl s_client -showcerts -connect api.stripe.com:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > api.stripe.cert
openssl pkcs12 -export -in api.stripe.com -out ewallet.p12 -nokeys
```
