# Machine prep

Add pre-required software to speed up actual installation process

```bash
sudo yum install -y epel-release

sudo yum install \
  libaio \
  bc \
  perl \
  git \
  firewalld \
  which \
  net-tools \
  htop \
  sudo \
  rlwrap \
  certbot \
  unzip \
  java \
  haveged -y
  
sudo systemctl enable haveged
sudo systemctl start haveged

```

Pull the installation media

```
s3cmd get s3://ausoug2016files/apex_5.0.3_en.zip
s3cmd get s3://ausoug2016files/apex_5.0.4_en.zip
s3cmd get s3://ausoug2016files/oracle-xe-11.2.0-1.0.x86_64.rpm.zip
s3cmd get s3://ausoug2016files/ords.3.0.6.176.08.46.zip
s3cmd get s3://ausoug2016files/ords.3.0.8.277.08.01.zip

```