# OXAR sample configurations

Repository created as part of my presentation for AUSOUG Connect 2016

* **public.properties** - A more secure configuration. Used for e.g. on a public internet site
* **private.properties** - A less secure configuration. Used for e.g. on a private machine through VirtualBox or other