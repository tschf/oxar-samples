#!/bin/bash
expdp system/Pha9puTh full=y dumpfile=backup.dmp logfile=backup.log
cd /u01/app/oracle/admin/XE/dpdump/
tar czvf backup.tgz backup.dmp backup.log --remove-files
s3cmd sync /u01/app/oracle/admin/XE/dpdump/ s3://ausoug2016bk --delete-removed
